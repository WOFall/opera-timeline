Live at <https://wofall.bitbucket.io/opera_timeline.html>
    and <https://wofall.bitbucket.io/firefox_timeline.html>

This script was primarily written for this project, but if you think it might
be useful to you, this readme details the general (messy) format of data.txt.
Consider checking out my data.txt as an example.

You'll need timeline.go and template.svg, and of course your own data.txt.
You will also need Go installed <http://golang.org>. To generate, run:
$ go run timeline.go

These variables are placed at the top of the file and control global settings.
All are optional.

	title = Timeline
		* text shown on browser tab or window decorations

	desc =
		* description (eg. shown at bottom of wikimedia file pages)

	data =
		* url to the dataset

	colorLine = #AAA
		* color of the lines

	colorDots = gray
		* color of the release markers (I suggest you personalize this)

	colorMisc = lightgray
		* color of "atypical" markers

	rows = 1000
		* number of series to print before looping back to the top

	height = 260
		* height of the document (sans overview) in pixels

	scale = 1
		* default scale to be applied in javascript enabled viewer

	details = 0.4
		* scale required to show higher detail (minor version text)


These variables are placed inside a series and control that series.

	row = <increments from 0>
		* optionally reset the row counter to the given value

	ongoing = false
		* when true, ends the series in a dashed line

	atypical = false
		* series contains only builds somehow unlike standard releases
		  (such builds are displayed along the bottom of the timeline)

You cannot set different colors for each series. Sorry :)


A series is started with "--" followed by an optional name. Names must be
unique - and also unique with IDs alrealy used by template.svg, although you
shouldn't need to worry about this. Names cannot contain ", <, or & at least,
and probably some more, so be sensible and stick to alphanumeric, _, and -.


A data point may look like one of the following:

YYYY-MM-DD: VER
	* a circle with subtext VER

YYYY-MM-DD: VER TYPE
	* a box containing text TYPE

YYYY-MM-DD: VER Milestone
	* a funky major version marker

YYYY-MM-DD: VER Milestone*
	* as above, but also shown on overview

All of these forms may be followed by a http/https URL
(and because of laziness, TYPE may not contain the text "http").
VER may not contain spaces, but you can use &#x20; if really needed.


Some modifiers can be attached to VER

^VER
	* place text above line, instead of default below
<VER
	* left align text to the point (anchor at text start)
VER>
	* right align text to the point (anchor at text end)

Think of ^, <, and > as arrows. You can also combine them as ^<VER or ^VER>.
(hint: they can also be used without VER text if a blank VER is needed.)


You can attach a comment above a data point by starting the line with a #.
Comment are copied into the final svg. Use = instead for anonymous comments.
You can attach a label below a data point by starting the line with a *.


That's all! Note that the script xml-escapes various parts for you, but not all
of them (for example VER), so don't trust against injection.

package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"
)

const (
	mMilestone = variant(48 + iota) // doesn't need to be printable, but why not
	mBeta
	mMinor
	mAtypical
	mLabel
)

var (
	templateFile = flag.String("template", "template.svg", "override the default template path")
	dataFile     = flag.String("data", "data.txt", "file containing the list of points")
	outFile      = flag.String("out", "timeline.svg", "file to create")
)

var svgTmpl, yearTmpl, ptTmpl, gTmpl *template.Template

var gConfigMap = map[string]string{
	"title":     "Timeline",
	"colorline": "#AAA",
	"colordots": "gray",
	"colormisc": "lightgray",
	"rows":      "1000",
	"height":    "260",
	"scale":     "1",
	"details":   "0.4",
}

type variant string

func (v variant) Is(s string) bool {
	return string(v) == s
}

type point struct {
	Comment []string
	Date,
	Url,
	Ver,
	Name,
	Typebox string
	X        int
	Width    int // for labels
	Keypoint bool
	LVer     bool   // milestone version number is long
	Dodge    bool   // need to dodge nearby point
	Anchor   string // text anchor
	Time     time.Time
	Tmpl     variant
}

type points []*point

func (ps points) Len() int           { return len(ps) }
func (ps points) Less(i, j int) bool { return ps[i].Time.Before(ps[j].Time) }
func (ps points) Swap(i, j int)      { ps[i], ps[j] = ps[j], ps[i] }

type series struct {
	name     string
	points   chan *point
	settings chan map[string]string
}

type group struct {
	Name       string
	Y          int
	Start, End int
	Points     bytes.Buffer
	Line       bool
	Ongoing    bool
}

type svgData struct {
	Width int
	Zero  int
	Cfg   map[string]string
	Years,
	Groups bytes.Buffer
}

type yearData struct {
	X, Year int
}

func init() {
	var err error
	svgTmpl, err = template.ParseFiles(*templateFile)
	must(err)
	yearTmpl = template.Must(template.New("yearTmpl").Parse(year_tmpl))
	ptTmpl = template.Must(template.New("ptTmpl").Parse(string(pt_tmpl)))
	gTmpl = template.Must(template.New("gTmpl").Parse(g_tmpl))
}

func main() {
	flag.Parse()

	seriesCh := make(chan *series, 10)
	file, err := ioutil.ReadFile(*dataFile)
	must(err)
	sreader := bytes.NewReader(file)

	go parse(bufio.NewReader(sreader), seriesCh)
	build(seriesCh)
	fmt.Println("Wrote", *outFile)
}

func istrue(s string) bool {
	switch strings.ToLower(s) {
	case "", "0", "false", "no":
		return false
	}
	return true
}

func must(err interface{}) {
	if err != nil {
		log.Fatal(err)
	}
}

func quantize_3(n int, size int) int {
	bucket := float64(size) / 3
	c := n/int(bucket) + 1
	// two thirds is awkward, just round it up
	if c%3 == 2 {
		c++
	}
	return int(float64(c) * bucket)
}

// fill out some extra info that parse doesn't
func (p *point) finish(baseTime time.Time) {
	p.Date = p.Time.Format("Jan 02, 2006")
	p.X = timeToPt(baseTime, p.Time)
	// decide which template to use
	if p.Tmpl != "" {
		// already decided
	} else if p.Name == "" {
		p.Tmpl = mMinor
	} else if i := strings.Index(strings.ToLower(p.Name), "milestone"); i >= 0 {
		p.Tmpl = mMilestone
		p.LVer = len(p.Ver) > 4
		if i += len("milestone"); i < len(p.Name) && p.Name[i] == '*' {
			p.Keypoint = true
		}
	} else {
		p.Tmpl = mBeta
		p.LVer = len(p.Name) > 6
	}
	// shift up
	if len(p.Ver) > 0 {
		if p.Ver[0] == '^' {
			p.Dodge = true
			p.Ver = p.Ver[1:]
		}
	}
	if l := len(p.Ver); l > 0 {
		if p.Ver[0] == '<' {
			p.Ver = p.Ver[1:]
			p.Anchor = "start"
		} else if p.Ver[l-1] == '>' {
			p.Ver = p.Ver[:l-1]
			p.Anchor = "end"
		}
	}
}

func timeToPt(baseTime, t time.Time) int {
	// adjust by 1px instead of sitting on month boundaries
	adj := 1
	// make a special "between" day for Feb 29
	if t.Month() == time.February && t.Day() == 29 {
		adj--
	}
	neg := 1
	if baseTime.After(t) {
		baseTime, t = t, baseTime
		neg = -1
	}
	days := int(t.Sub(baseTime).Hours() / 24)
	days -= leaps(baseTime, t)
	return neg*days*2 + adj
}

// the number of leap-days between two dates
func leaps(t1, t2 time.Time) (leaps int) {
	if t1.After(t2) {
		t1, t2 = t2, t1
	}
	// most recent year
	if leapYear(t2.Year()) && t2.Month() > time.February &&
		(t1.Month() <= time.February || t1.Year() < t2.Year()) {
		leaps++
	}
	if t1.Year() == t2.Year() {
		return leaps
	}
	// oldest year
	if leapYear(t1.Year()) && t1.Month() <= time.February {
		leaps++
	}
	// in-between years
	for y1 := t1.Year() + 1; y1 < t2.Year(); y1++ {
		if leapYear(y1) {
			leaps++
		}
	}
	return leaps
}

func leapYear(y int) bool {
	feb, err := time.Parse("2006-01-02", fmt.Sprintf("%4d-02-29", y))
	// go<1.6 does not give an error, but parses as March 1
	return err == nil && feb.Month() == time.February
}

// eats to first "delim", and also chop "delim" off "rest"
func eat(in []byte, delim string) (ate, rest []byte) {
	d := []byte(delim)
	if len(in) == 0 {
		return nil, nil
	}
	pair := bytes.SplitN(in, d, 2)
	if len(pair) > 1 {
		return pair[0], pair[1]
	} else {
		return nil, pair[0]
	}
}

// eats to just before first "delim"
func chop(in []byte, delim string) (ate, rest []byte) {
	d := []byte(delim)
	li := bytes.Index(in, d)
	if li >= 0 {
		return in[:li], in[li:]
	} else {
		return in, nil
	}
}

func parse(in *bufio.Reader, seriesCh chan<- *series) {
	anonCount := 0
	nextName := func() string {
		anonCount++
		return "unnamed" + fmt.Sprint(anonCount)
	}
	var points chan *point
	var settings chan map[string]string
	var configMap map[string]string
	seenData := false
	pt := &point{}
	for reading := true; reading; {
		line, err := in.ReadBytes('\n')
		if err == io.EOF {
			reading = false
		} else if err != nil {
			log.Fatal(err)
		}
		line = bytes.TrimSpace(line)
		if len(line) == 0 {
			continue
		}
		c := line[0]
		if bytes.HasPrefix(line, []byte("--")) {
			// Series
			if seenData {
				points <- pt
				pt = &point{}
				seenData = false
			}
			if points != nil {
				close(points)
				settings <- configMap
				close(settings)
			}
			points = make(chan *point)
			settings = make(chan map[string]string)
			configMap = map[string]string{}
			line = bytes.TrimSpace(line[len("--"):])
			name := string(line)
			if name == "" {
				name = nextName()
			}
			seriesCh <- &series{
				name:     name,
				points:   points,
				settings: settings,
			}
		} else if c == '#' {
			// Comment
			if seenData {
				points <- pt
				pt = &point{}
				seenData = false
			}
			if points != nil {
				line = bytes.TrimSpace(line[1:])
				pt.Comment = append(pt.Comment, string(line))
			}
		} else if c >= '0' && c <= '9' {
			// Point
			if points == nil {
				log.Fatal("Error: no series declared. Declare a series with -- first.")
			}
			if seenData {
				points <- pt
				pt = &point{}
			}
			seenData = true
			date, line := eat(line, ":")
			line = bytes.TrimSpace(line)
			time, err := time.Parse("2006-01-02", string(date))
			if err != nil {
				log.Fatal("Error: cannot parse date. Missing colon?\n  ", err)
			}
			pt.Time = time
			ver, line := eat(line, " ")
			if ver == nil {
				ver, line = line, ver
			}
			pt.Ver = string(ver)
			name, line := chop(line, "http")
			pt.Name = string(bytes.TrimSpace(name))
			pt.Url = string(line)
		} else if c == '*' {
			// Label
			if !seenData {
				log.Fatal("error: unexpected label: ", string(line))
			}
			seenData = false
			line = bytes.TrimSpace(line[1:])
			time := pt.Time
			comment := pt.Comment
			name, line := chop(line, "http")
			nameStr := string(bytes.TrimSpace(name))
			points <- pt
			points <- &point{
				Tmpl:    mLabel,
				Time:    time,
				Name:    nameStr,
				Width:   len(nameStr)*6 + 20, // very rough guestimation
				Url:     string(line),
				Comment: comment,
			}
			pt = &point{}
		} else if token, line := eat(line, "="); token != nil {
			// Variable
			tok := string(bytes.ToLower(bytes.TrimSpace(token)))
			val := string(bytes.TrimSpace(line))
			if tok == "" {
				continue
			}
			if settings == nil { // before the first series - global setting
				gConfigMap[tok] = val
			} else {
				configMap[tok] = val
			}
		} else {
			log.Println("Ignoring line:", string(line))
		}
	}
	if seenData {
		points <- pt
	}
	if points != nil {
		close(points)
		settings <- configMap
		close(settings)
	}
	close(seriesCh)
}

// outputs the svg
func build(seriesCh <-chan *series) {
	var gOut, yearsOut, svgOut bytes.Buffer
	var zeroTime, oldestTime, newestTime *time.Time
	series_i := -1
	for s := range seriesCh {
		var err error
		var ptOut bytes.Buffer
		var pointsList points
		for pt := range s.points {
			pointsList = append(pointsList, pt)
		}
		configMap := <-s.settings
		if row_str, ok := configMap["row"]; ok {
			series_i, err = strconv.Atoi(row_str)
			must(err)
		} else {
			rows, err := strconv.Atoi(gConfigMap["rows"])
			must(err)
			series_i = (series_i + 1) % rows
		}
		if len(pointsList) == 0 {
			continue
		}
		if !sort.IsSorted(pointsList) {
			log.Println("Warning: dates not sorted in series", s.name, " - sorting")
			sort.Sort(pointsList)
		}
		baseYear := pointsList[0].Time.Year()
		baseTime, err := time.Parse("2006", fmt.Sprint(baseYear))
		must(err)
		if zeroTime == nil {
			t := baseTime
			zeroTime = &t // year where x = 0
		}
		atypical := istrue(configMap["atypical"])
		for _, pt := range pointsList {
			if atypical {
				pt.Tmpl = mAtypical
			}
			pt.finish(*zeroTime)
			must(ptTmpl.ExecuteTemplate(&ptOut, "ptTmpl", pt))
		}
		endTime := pointsList[len(pointsList)-1].Time.AddDate(0, 0, 15)
		if oldestTime == nil || baseTime.Before(*oldestTime) {
			t := baseTime
			oldestTime = &t
		}
		if newestTime == nil || endTime.After(*newestTime) {
			t := endTime
			newestTime = &t
		}
		must(gTmpl.ExecuteTemplate(&gOut, "gTmpl", &group{
			Name: s.name,
			Y: func(atypical bool, series_i int) int {
				if atypical {
					return 170
				} else {
					return 95 + (series_i * 75)
				}
			}(atypical, series_i),
			Start:   timeToPt(*zeroTime, pointsList[0].Time.AddDate(0, 0, -25)),
			End:     timeToPt(*zeroTime, endTime),
			Line:    !atypical,
			Points:  ptOut,
			Ongoing: istrue(configMap["ongoing"]),
		}))
	}

	width := quantize_3(timeToPt(*oldestTime, *newestTime)-1+30, 730)
	oldestTimePt := timeToPt(*zeroTime, *oldestTime) - 1
	var years []*yearData
	for x, y := oldestTimePt, oldestTime.Year(); x < width+oldestTimePt; x, y = x+730, y+1 {
		years = append(years, &yearData{
			X:    x,
			Year: y,
		})
	}
	must(yearTmpl.ExecuteTemplate(&yearsOut, "yearTmpl", years))

	must(svgTmpl.ExecuteTemplate(&svgOut, *templateFile, &svgData{
		Cfg:    gConfigMap,
		Width:  width,
		Zero:   oldestTimePt,
		Years:  yearsOut,
		Groups: gOut,
	}))
	must(ioutil.WriteFile(*outFile, svgOut.Bytes(), 0644))
}

// templates turned out to be a lot less flexible than I planned for... 0:)
const pt_tmpl = `
			<{{with .Url}}a xlink:href="{{html .}}"{{else}}g{{end}}>
				<title>{{.Date}}{{with .Comment}}
{{end}}{{range .Comment}}
{{.}}{{end}}</title>
{{if "` + mMinor + `" |.Tmpl.Is}}` +
	`				<circle cx="{{.X}}" r="3"/>
				<text x="{{.X}}" dy="{{with .Dodge}}-9{{else}}16{{end}}" class="maint"{{with .Anchor}} style="text-anchor:{{.}};"{{end}}>{{.Ver}}</text>
{{else}}{{if "` + mBeta + `" |.Tmpl.Is}}` +
	`				<circle cx="{{.X}}" r="2" class="beta"/>
				<text x="{{.X}}" dy="-14" class="version">{{.Ver}}</text>
				<text x="{{.X}}" dy="-5" class="version">{{html .Name}}</text>
{{else}}{{if "` + mMilestone + `" |.Tmpl.Is}}` +
	`				<use x="{{.X}}" xlink:href="#{{with .LVer}}l{{end}}majorlabel{{if .Dodge}}-dodger{{end}}"/>
				<text x="{{.X}}" dy="-09" class="final{{if .Keypoint}} keypoint{{end}}{{if .Dodge}} dodger{{end}}">{{.Ver}}</text>
{{else}}{{if "` + mAtypical + `" |.Tmpl.Is}}` +
	`				<circle cx="{{.X}}" cy="{{with .Dodge}}64{{else}}46{{end}}" r="3" class="atypical"/>
				<text x="{{.X}}" dy="{{with.Dodge}}68{{else}}50{{end}}" dx="8" class="atypical">{{html .Name}}</text>
{{else}}{{if "` + mLabel + `" |.Tmpl.Is}}` +
	`				<rect x="{{.X}}" y="21" height="17" width="{{.Width}}" transform="translate(6)" style="fill:white; fill-opacity:0.7"/>
				<circle cx="{{.X}}" cy="30" r="3" class="xscale"/>
				<text x="{{.X}}" dy="34" dx="8" class="atypical" style="font-weight:bold;">{{html .Name}}</text>
{{end}}{{end}}{{end}}{{end}}{{end}}			</{{with .Url}}a{{else}}g{{end}}>
`

const g_tmpl = `
		<g id="{{.Name}}" transform="translate(0, {{.Y}})" class="xscale">{{if .Line}}
			<use xlink:href="#smallIn" x="{{.Start}}"/>
			<line x1="{{.Start}}" x2="{{.End}}"/>
			<use xlink:href="{{with .Ongoing}}#dashOut{{else}}#smallOut{{end}}" x="{{.End}}"/>
{{end}}{{.Points}}		</g>

`

const year_tmpl = `{{range .}}			<tspan x="{{.X}}">{{.Year}}</tspan>
{{end}}`

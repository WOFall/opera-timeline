all: opera_timeline.svg opera_timeline.static.svg firefox_timeline.svg

opera_timeline.svg: opera_data.txt opera_tweak.diff template.svg timeline.go
	go run timeline.go -data $< -out $@
	patch < opera_tweak.diff

opera_timeline.static.svg: opera_timeline.svg opera_scaled.diff unscript.sh
	./unscript.sh

firefox_timeline.svg: firefox_data.txt template.svg timeline.go
	go run timeline.go -data $< -out $@

deploy: all
	@if ! git diff --quiet HEAD; then echo "Commit first!"; false; fi
	git push
	@cp opera_timeline{,.static}.svg firefox_timeline.svg ~/code/wofall.bitbucket.org/
	@cp opera_timeline.static.svg last.static.svg
	@echo pushing update to wofall.bitbucket.org
	@cd ~/code/wofall.bitbucket.org/ && \
	    git commit -am "update" && git push
	@python pywikibot/pwb.py upload -filename:Opera_Release_Timeline.svg -summary:"$$(git show -s --format=%s)" opera_timeline.static.svg "update"

#!/bin/sh -e
# Because Wikimedia Commons doesn't allow uploads containing scripts, and
# because it does not like very wide images.

cp opera_timeline.svg opera_scaled.svg
patch < opera_scaled.diff

/usr/bin/chromium --headless --no-sandbox --dump-dom opera_scaled.svg > opera_scaled_dom.svg

sed -e"s/<\/desc>/\n\tAn interactive version of this timeline can be found at\n\thttps:\/\/wofall.bitbucket.io\/opera_timeline.html&/" \
	-e"/<script/,/<\/script/d" \
	-e"/<g id=\"labs\"/,/<\/g>/d" \
	opera_scaled_dom.svg > opera_timeline.static.svg
